package com.atlassian.connect.play.java;

public interface AcHost
{
    Long getId();

    String getKey();

    String getName();

    String getDescription();

    String getBaseUrl();

    String getPublicKey();

    String getSharedSecret();

    String getConsumerInfoUrl();
}
