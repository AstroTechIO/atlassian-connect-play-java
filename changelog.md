# Changelog - Play Java Module for Atlassian Connect

## 0.7.1

* changed to use new AUI CDN

## 0.7.0

* **BREAKING**

  * Switch from OAuth to JWT for authentication

    * See [Authentication Documentation](https://developer.atlassian.com/static/connect/docs/pages/concepts/authentication.html) for details

  * Switch from XML Descriptors to JSON.

    * See [atlassian-connect documentation](https://developer.atlassian.com/static/connect/docs/) for details.

## 0.6.4 
* **BREAKING** 
  * Upgrade to Play 2.2
